const winston = require('../../winston')
const fs = require('fs');
const moment = require('moment')

let estaTrabajando = false
let espera = null
let testFolder = process.env.ARIFACE2_DEMONIO_LISTAFICHEROS_DIRECTORIO || './'

const demonioListarFicherosApi = {
    run: () => {
        if (estaTrabajando) return;
        (async () => {
            try {
                estaTrabajando = true;
                // Aquí es donde se hace el trabajo.
                winston.info(moment().format('YYYYMMDD HH:mm:ss') + " -- demonioListarFicheros start")
                fs.readdirSync(testFolder).forEach(file => {
                    winston.info(file);
                  });
                winston.info(moment().format('YYYYMMDD HH:mm:ss') + " -- demonioListarFicheros end")
                estaTrabajando = false
            } catch (err) {
                estaTrabajando = false
                winston.error("Error demonioListarFicheros: " + err.message)
            }
        })()
    }
}

module.exports = demonioListarFicherosApi;