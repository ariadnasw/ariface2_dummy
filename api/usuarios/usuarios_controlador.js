const express = require('express');
var router = express.Router();

const usuarios_mysql = require('./usuarios_mysql')

router.post('/', async (req, res, next) => {
    try {
        if (!req.body.email) {
            res.status(400).json("Solicitud malformada (email)")
        }
        usuarios_mysql.postUsuariosMysql(req.body).then((result) => {
            res.json(result)
        }).catch(err => next(err))
    } catch (error) {
        next(error);
    }
});

router.get("/", async (req, res, next) => {
    try {
        usuarios_mysql.getUsuariosMsql()
            .then((result) => {
                res.json(result)
        }).catch(err => next(err))
    } catch (error) {
        next(error);
    }
})

router.get("/:id", async (req, res, next) => {
    try {
        usuarios_mysql.getUsuariosMsqlById(req.params.id)
            .then((result) => {
                res.json(result)
        }).catch(err => next(err))
    } catch (error) {
        next(error);
    }
})


router.put('/', async (req, res, next) => {
    console.log('PUT /', req.body)
    try {
        if (!req.body.usuarioId) {
            res.status(400).json("Solicitud malformada (usuarioId∫)")
        }
        usuarios_mysql.putUsuariosMsql(req.body).then((result) => {
            res.json(result)
        }).catch(err => next(err))
    } catch (error) {
        next(error);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        if (!req.params.id) {
            //res.status(400).json("Solicitud malformada (usuarioId∫)")
        }
        usuarios_mysql.deleteUsuariosMsqlById(req.params.id).then((result) => {
            res.json(result)
        }).catch(err => next(err))
    } catch (error) {
        next(error);
    }
});

module.exports = router;