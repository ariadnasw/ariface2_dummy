const mysql = require('mysql2/promise') 

const usuarios_mysql = {
    postUsuariosMysql: async (usuario) => {
        console.log("Usuario", usuario)
        // Vamos a devolver una promesa
        return new Promise(async (resolve, reject) => {
            let conn = undefined // conn lleva la conexión con la base de datos
            try {
                conn = await mysql.createConnection(obtenerConfiguracion());
                const [resp] = await conn.query("INSERT INTO usuarios SET ?", usuario)
                console.log(resp)
                usuario.usuarioId = resp.insertId
                await conn.end()
                resolve (usuario)
            } catch (error) {
                if (conn) await conn.end()
                reject (error)
            }
        })
    },
    getUsuariosMsql: async () => {
        // Vamos a devolver una promesa
        return new Promise(async (resolve, reject) => {
            let conn = undefined // conn lleva la conexión con la base de datos
            try {
                conn = await mysql.createConnection(obtenerConfiguracion());
                const [resp] = await conn.query("SELECT * FROM usuarios")
                await conn.end()
                resolve (resp)
            } catch (error) {
                if (conn) await conn.end()
                reject (error)
            }
            })
    },
    getUsuariosMsqlById: async (id) => {
        // Vamos a devolver una promesa
        return new Promise(async (resolve, reject) => {
            let conn = undefined // conn lleva la conexión con la base de datos
            try {
                conn = await mysql.createConnection(obtenerConfiguracion());
                const sql = `SELECT * FROM usuarios WHERE usuarioId=${id}`
                const [resp] = await conn.query(sql)
                await conn.end()
                resolve (resp)
            } catch (error) {
                if (conn) await conn.end()
                reject (error)
            }
            })
    },
    putUsuariosMsql: async (usuario) => {
        console.log('Entre en putUsuariosMysql')
        // Vamos a devolver una promesa
        return new Promise(async (resolve, reject) => {
            let conn = undefined // conn lleva la conexión con la base de datos
            try {
                conn = await mysql.createConnection(obtenerConfiguracion());
                let sql = 'UPDATE usuarios SET ? WHERE usuarioId = ?'
                const [resp] = await conn.query(sql, [usuario, usuario.usuarioId])
                console.log(resp)
                await conn.end()
                resolve (resp)
            } catch (error) {
                if (conn) await conn.end()
                reject (error)
            }
            })
    },
    getUsuariosMsqlById: async (id) => {
        // Vamos a devolver una promesa
        return new Promise(async (resolve, reject) => {
            let conn = undefined // conn lleva la conexión con la base de datos
            try {
                conn = await mysql.createConnection(obtenerConfiguracion());
                const sql = `SELECT * FROM usuarios WHERE usuarioId=${id}`
                const [resp] = await conn.query(sql)
                await conn.end()
                resolve (resp)
            } catch (error) {
                if (conn) await conn.end()
                reject (error)
            }
            })
    },
    deleteUsuariosMsqlById: async (id) => {
        // Vamos a devolver una promesa
        return new Promise(async (resolve, reject) => {
            let conn = undefined // conn lleva la conexión con la base de datos
            try {
                conn = await mysql.createConnection(obtenerConfiguracion());
                const sql = `DELETE FROM usuarios WHERE usuarioId=${id}`
                const [resp] = await conn.query(sql)
                await conn.end()
                resolve (resp)
            } catch (error) {
                if (conn) await conn.end()
                reject (error)
            }
            })
    },
}

const obtenerConfiguracion = () => {
    return configuracion = {
        host: process.env.ARIFACE2_MYSQL_HOST,
        port: process.env.ARIFACE2_MYSQL_PORT,
        user: process.env.ARIFACE2_MYSQL_USER,
        password: process.env.ARIFACE2_MYSQL_PASSWORD,
        database: process.env.ARIFACE2_MYSQL_DATABASE
    }
}
module.exports = usuarios_mysql