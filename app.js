const express = require('express');
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
const winston = require('./winston');
const demonioListarFicherosApi = require('../ariface2_dummy/api/demonios/demonio_listar_ficheros');

dotenv.config()

const app = express();


const appServer = {
    createServer: () => {
        console.log('Create server');
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());
        app.use('/version', require('./api/version/version_controlador'));
        app.use('/usuarios', require('./api/usuarios/usuarios_controlador'));
        app.use((error, req, res, next) => {
            res.status(error.status || 500);
            res.json({
                error: {
                    message: error.message
                }
            });
        });
    },
    lauchServer: () => {
        winston.info('Lauch server');
        const port = process.env.ARIFACE2_PORT || 49152
        winston.info(`Server ariface2_dummy listen at port ${port}`)
        var server = app.listen(port, () => {})
    },
    lanzarDemonioListarFicheros: () => {
        setInterval(demonioListarFicherosApi.run, process.env.ARIFACE2_DEMONIO_LISTAFICHEROS_DELAY || 50000)
    }
}

module.exports = appServer;